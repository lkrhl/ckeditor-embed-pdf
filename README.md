# PDF embed plugin for CKEditor 4

Install steps:

- add plugin to CKEditor:
    ```javascript
    CKEDITOR.plugins.addExternal('embedpdf', 'path/to/embedpdf/', 'plugin.js');
    ```
- add ```EmbedPDF``` to the list of toolbar buttons