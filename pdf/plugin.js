CKEDITOR.plugins.add('embedpdf', {
  lang: ['en', 'hr', 'ba', 'sr-latn'],
  init: function (editor) {
    var fileUrl = 'CKfileUrl';
    var lang = editor.lang.embedpdf;
    var embedUrl = '';

    editor.addCommand('embedpdf', new CKEDITOR.dialogCommand('embedpdf', {
      allowedContent: 'iframe[*]; p;'
    }));

    editor.ui.addButton('EmbedPDF', {
      label: lang.button,
      toolbar: 'insert',
      command: 'embedpdf',
      icon: this.path + 'icons/64x64.png'
    });

    CKEDITOR.dialog.add('embedpdf', function () {
      return {
        title: lang.title,
        minWidth: 600,
        minHeight: 100,
        contents: [
          {
            id: 'embedpdfPlugin',
            expand: true,
            elements: [
              {
                type: 'hbox',
                widths: ['85%', '15%', '0%'],
                children: [
                  {
                    id: fileUrl,
                    type: 'text',
                    label: lang.fileUrl,
                    validate: CKEDITOR.dialog.validate.notEmpty(lang.fileUrlEmpty),
                    onChange: function () {
                        embedUrl = this.getValue();
                    }
                  }
                ]
              }
            ]
          }
        ],
        onOk: function () {
          var content = '<p><iframe src="' + embedUrl + '" style="width: 100%; height: 60vh;"></iframe></p>';
          var element = CKEDITOR.dom.element.createFromHtml(content);
          this.getParentEditor().insertElement(element);
        }
      };
    });
  }
});
