CKEDITOR.plugins.setLang('embedpdf', 'en', {
  button: 'Embed PDF document',
  title: 'Embed PDF document',
  fileUrl: 'Enter a link to a document',
  fileUrlEmpty: 'Document link is required!'
});
