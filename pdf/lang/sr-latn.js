CKEDITOR.plugins.setLang('embedpdf', 'sr-latn', {
  button: 'Dodaj PDF dokument',
  title: 'Dodaj PDF dokument',
  fileUrl: 'Unesi link na dokument',
  fileUrlEmpty: 'Link na dokument je obavezan!'
});
